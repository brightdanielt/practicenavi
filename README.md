一開始只是想練習 JetPack Navigation，但寫到一半發現沒有具體的ＡＰＰ功能，很難實作 Navigation api，
以顯現其方便與強大，左思右想後決定加入 TMDB 的電影資訊查詢功能，是過去串接過的 api，比較熟悉，
如此一來得以使用：

1. Navigation( naviGraph + safeArg + ~~sharedViewModel~~ ) 
2. Retrofit2 + Coroutine
3. ViewModelFactory + ViewModel + ViewModelScope 
4. Databinding + bindAdapter
5. Room
6. Dagger2

使用者操作畫面：
1. 清單列出電影名稱
2. 點擊清單項目顯示詳細電影資訊

11/19<br>
    花了三天的時間查看了 Dagger 相關的文章，雖然過去看過也練習過 Dagger，但差別在於這次是跟著 Google 的 codeLab 學習，
    很清楚的說明各種 annotation 的正確用法，並加入範例與 Dependence graph，真是妙不可言，請參考這篇：
    [Dependency Injection Guidance on Android — ADS 2019]，裡面已包含許多文件與 codeLab 連結 ：）
    接下來，我將加入 Dagger2 到該專案，並在各個 annotation 附上功能註解，同時繪製 dependence graph，好期待喔！！
    
11/20<br>
    DI 導入完成 100% 了(大概吧，目前沒有問題)，過程中，比較棘手的是 ViewModelFactory，雖然照著一些範例改寫就成功了，但有些寫法是不甚理解的，
    另外手邊有 dependence graph 手繪版，接著把它製作成美美的電子圖檔。
    
11/21<br>
    原本想使用 Omnigraffle 繪製 dependence graph，不過發現試用到期，於是改用[Cacoo]，
    放在 Cacoo 的圖表連結:[Dependence graph]請以 Cacoo 的連結為正確參考，也能在這預覽 png 圖檔:<br><img src="dependence_graph.png" width="520" height="240">
    
11/30<br>
    昨晚體驗 [CodeLab - Paging]，了解 paging 機制的瞬間，驚嘆了好幾下！！
    同時 paging 可搭配 Room、LiveData、RecyclerView 的 adapter。
    今天我們要導入 paging 至本專案：）
    首先繪製清 MovieItem 的 data flow，非常幸運，本專案的架構與該 sample 非常相似，直接照用！<br>
    <img width = 400 height =200 url=https://codelabs.developers.google.com/codelabs/android-paging/img/511a702ae4af43cd.png><br>
    1. Network request (Gson) -> Room (List<MovieItem>)
    2. Db query (DataSource.Factory<Int,MovieItem>) -> Repository (DataSource.Factory<Int,MovieItem>)
     -> ViewModel(LiveData<PagedList<MovieItem>>)
     以下的過程由 DataBinding 完成，無法直接看到
     -> Observer (PagedList<MovieItem>) 
     -> PagedAdapter (PageList<MovieItem>)


[CodeLab - Paging]:https://codelabs.developers.google.com/codelabs/android-paging/index.html?index=..%2F..ads19#0
[Data flow of paging sample]:https://codelabs.developers.google.com/codelabs/android-paging/img/511a702ae4af43cd.png
[Dependence graph]:https://cacoo.com/diagrams/G2eXviQewwIvo80r/3E669
[Dependency Injection Guidance on Android — ADS 2019]:https://medium.com/androiddevelopers/dependency-injection-guidance-on-android-ads-2019-b0b56d774bc2
[Cacoo]:https://cacoo.com/
    