package com.cauliflower.danielt.practicenavi.di

import android.content.Context
import androidx.room.Room
import com.cauliflower.danielt.practicenavi.BaseRepository
import com.cauliflower.danielt.practicenavi.MovieDao
import com.cauliflower.danielt.practicenavi.MovieDatabase
import com.cauliflower.danielt.practicenavi.MovieRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module(includes = [AppModuleBinds::class, NetworkModule::class])
object AppModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideMovieDatabase(context: Context): MovieDatabase {
        return Room.databaseBuilder(
            context.applicationContext,
            MovieDatabase::class.java,
            "Movies.db"
        ).build()
    }

    @JvmStatic
    @Provides
    @Singleton
    fun provideMovieDao(movieDatabase: MovieDatabase): MovieDao {
        return movieDatabase.movieDao()
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideIoDispatcher() = Dispatchers.IO
}

@Module
abstract class AppModuleBinds {

    /**
     * Use Binds to tell Dagger which implementation it need to use when providing an interface.
     * Here the interface is BaseRepository and implementation is MovieRepository
     * */
    @Singleton
    @Binds
    abstract fun bindsMovieRepository(repository: MovieRepository): BaseRepository
}