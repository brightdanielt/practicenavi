package com.cauliflower.danielt.practicenavi

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cauliflower.danielt.practicenavi.obj.FavoriteMovie
import com.cauliflower.danielt.practicenavi.obj.MovieItem

@Dao
interface MovieDao {

    @Query("SELECT * FROM favoritemovies")
    suspend fun getFavoriteMovies(): List<FavoriteMovie>

    /**
     * @return 0 -> it's not your favorite movie.
     * @return 1 -> it is your favorite movie.
     * */
    @Query("SELECT COUNT() FROM favoritemovies WHERE movieId =:movieId")
    suspend fun isFavoriteMovie(movieId: String): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addFavoriteMovie(favoriteMovie: FavoriteMovie)

    @Query("DELETE FROM favoritemovies WHERE movieId =:movieId")
    suspend fun removeFavoriteMovie(movieId: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun cacheMovies(movies: List<MovieItem>)

    //todo suspense and LiveData conflict?
    //FYI
    //https://stackoverflow.com/questions/58424939/kotlin-coroutine-suspend-error-with-room-datasource-factory
    @Query("SELECT * FROM movieItemLocalCache")
    fun getCachedMovies(): DataSource.Factory<Int, MovieItem>
}