package com.cauliflower.danielt.practicenavi.network

import androidx.paging.PagedList
import com.cauliflower.danielt.practicenavi.obj.MovieItem

/**
 * Let paging know what to do when onZeroItemsLoaded and onItemAtEndLoaded.
 * */
class MovieBoundaryCallback constructor(private val requestAndSaveMovies: () -> Unit) :
    PagedList.BoundaryCallback<MovieItem>() {

    override fun onZeroItemsLoaded() {
        super.onZeroItemsLoaded()
        requestAndSaveMovies()
    }

    override fun onItemAtEndLoaded(itemAtEnd: MovieItem) {
        super.onItemAtEndLoaded(itemAtEnd)
        requestAndSaveMovies()
    }

}