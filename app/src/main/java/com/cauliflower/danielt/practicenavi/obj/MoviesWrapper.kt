package com.cauliflower.danielt.practicenavi.obj

class MoviesWrapper {

    private var results: List<MovieItem> = emptyList()

    fun getMovieList(): List<MovieItem> {
        return results
    }

    fun setMovieList(movieList: List<MovieItem>) {
        this.results = movieList
    }
}