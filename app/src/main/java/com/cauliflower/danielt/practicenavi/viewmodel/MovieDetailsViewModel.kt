package com.cauliflower.danielt.practicenavi.viewmodel

import android.util.Log
import androidx.lifecycle.*
import com.cauliflower.danielt.practicenavi.MovieRepository
import com.cauliflower.danielt.practicenavi.Result
import com.cauliflower.danielt.practicenavi.obj.FavoriteMovie
import com.cauliflower.danielt.practicenavi.obj.MovieItem
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieDetailsViewModel @Inject constructor(
    private val movieRepository: MovieRepository
) : ViewModel() {

    private lateinit var movieId: String

    private val _movieItem = MutableLiveData<MovieItem?>()
    val movieItem: LiveData<MovieItem?> = _movieItem

    private val _isMovieLoading = MutableLiveData<Boolean>()
    val isMovieLoading: LiveData<Boolean> = _isMovieLoading

    /*var fullPosterPath = Transformations.map(movieItem) {
        BuildConfig.TMDB_IMAGE_BASE_URL + "/w500" + it.poster_path
    }*/
    var posterPath = Transformations.map(_movieItem) { it?.poster_path }

    val isEmptyMovie: LiveData<Boolean> = Transformations.map(_movieItem) { it == null }

    private val _isFavorite = MutableLiveData<Boolean>()
    val isFavorite: LiveData<Boolean> = _isFavorite

    /*init {
        loadMovie()
    }*/

    fun loadMovie(movieId: String) {
        this.movieId = movieId
        if (_isMovieLoading.value == true) return
        _isMovieLoading.value = true

        viewModelScope.launch {
            movieRepository.searchMovieById(movieId).let {
                when (it) {
                    is Result.Success -> onMovieAvailable(it.data)
                    is Result.Error -> onMovieNotAvailable().run {
                        Log.i(MovieDetailsViewModel::class.java.simpleName,it.toString()) }
                }
                _isMovieLoading.value = false
            }
        }
    }

    private fun onMovieAvailable(movieItem: MovieItem) {
        setMovieItem(movieItem)
        getFavorite()
    }

    private fun onMovieNotAvailable() {
        setMovieItem(null)
    }

    private fun setMovieItem(movieItem: MovieItem?) {
        _movieItem.value = movieItem
    }

    fun refresh() {
        loadMovie(movieId)
    }

    fun setFavorite() = viewModelScope.launch {
        movieItem.value?.let {
            if (_isFavorite.value == true) {
                movieRepository.removeFavoriteMovie(it.id.toString())
            } else {
                movieRepository.addFavoriteMovie(FavoriteMovie(it.id.toString()))
            }
        }
        getFavorite()
    }

    //Can not use coroutine and LiveData together in Room so we query it ourselves
    private fun getFavorite() = viewModelScope.launch {
        _isFavorite.value = (movieRepository.isFavoriteMovie(movieItem.value?.id.toString()) == 1)
    }

}
