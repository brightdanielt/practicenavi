package com.cauliflower.danielt.practicenavi.di

import com.cauliflower.danielt.practicenavi.BuildConfig
import com.cauliflower.danielt.practicenavi.network.RequestInterceptor
import com.cauliflower.danielt.practicenavi.network.TmdbService
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    private val CONNECTION_TIME_OUT: Long = 15 * 1000

    @Singleton
    @Provides
    fun provideTmdbService(retrofit: Retrofit): TmdbService {
        return retrofit.create(TmdbService::class.java)
    }

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder().run {
            baseUrl(BuildConfig.TMDB_API_BASE_URL)
            client(okHttpClient)
            addConverterFactory(GsonConverterFactory.create())
            build()
        }
    }

    /**
     * CoroutineCallAdapter was deprecated, believe it or not : )
     * So I won't implement it in build.gradle
     * https://github.com/JakeWharton/retrofit2-kotlin-coroutines-adapter
     * */
    @Singleton
    @Provides
    fun provideOkHttpClient(interceptor: RequestInterceptor): OkHttpClient {
        return OkHttpClient.Builder().run {
            if (BuildConfig.DEBUG) {
                addInterceptor(
                    HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
                )
            }
            addInterceptor(interceptor)
            connectTimeout(CONNECTION_TIME_OUT, TimeUnit.MILLISECONDS)
            build()
        }
    }

    @Singleton
    @Provides
    fun provideRequestInterceptor(): Interceptor {
        return RequestInterceptor()
    }
}