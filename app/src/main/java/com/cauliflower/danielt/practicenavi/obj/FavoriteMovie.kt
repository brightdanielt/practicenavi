package com.cauliflower.danielt.practicenavi.obj

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favoriteMovies")
class FavoriteMovie(

    @PrimaryKey
    @ColumnInfo(name = "movieId")
    val movieId: String

) {

}