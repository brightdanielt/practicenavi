package com.cauliflower.danielt.practicenavi

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.cauliflower.danielt.practicenavi.obj.FavoriteMovie
import com.cauliflower.danielt.practicenavi.obj.MovieItem

@Database(entities = [FavoriteMovie::class, MovieItem::class], version = 1)
@TypeConverters(MovieItemConverters::class)
abstract class MovieDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}