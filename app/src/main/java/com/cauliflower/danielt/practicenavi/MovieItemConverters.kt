package com.cauliflower.danielt.practicenavi

import androidx.room.TypeConverter
import com.cauliflower.danielt.practicenavi.obj.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * To save Entity which has other data classes in Room,
 * we should create TypeConverters for these data classes.
 * */
class MovieItemConverters {

    private val gson: Gson = Gson()

    private val genreListType = object : TypeToken<List<Genre?>?>() {}.type
    private val spokenLanguageListType = object : TypeToken<List<SpokenLanguage?>?>() {}.type
    private val productionCompanyListType = object : TypeToken<List<ProductionCompany?>?>() {}.type
    private val productionCountryListType = object : TypeToken<List<ProductionCountry?>?>() {}.type
    private val belongsToCollectionType = object : TypeToken<BelongsToCollection?>() {}.type

    @TypeConverter
    fun gsonToGenreList(json: String): List<Genre?>? {
        return gson.fromJson(json, genreListType)
    }

    @TypeConverter
    fun genreListToGson(genres: List<Genre?>?): String {
        return gson.toJson(genres)
    }

    @TypeConverter
    fun gsonToSpokenLanguageList(json: String): List<SpokenLanguage?>? {
        return gson.fromJson(json, spokenLanguageListType)
    }

    @TypeConverter
    fun spokenLanguageListToGson(spokenLanguageList: List<SpokenLanguage?>?): String {
        return gson.toJson(spokenLanguageList)
    }

    @TypeConverter
    fun gsonToProductionCompanyList(json: String): List<ProductionCompany?>? {
        return gson.fromJson(json, productionCompanyListType)
    }

    @TypeConverter
    fun productionCompanyListToGson(productionCompanyList: List<ProductionCompany?>?): String {
        return gson.toJson(productionCompanyList)
    }

    @TypeConverter
    fun gsonToProductionCountryList(json: String): List<ProductionCountry?>? {
        return gson.fromJson(json, productionCountryListType)
    }

    @TypeConverter
    fun productionCountryListToGson(productionCountryList: List<ProductionCountry?>?): String {
        return gson.toJson(productionCountryList)
    }

    @TypeConverter
    fun gsonToBelongsToCollection(json: String): BelongsToCollection? {
        return gson.fromJson(json, belongsToCollectionType)
    }

    @TypeConverter
    fun belongsToCollectionToGson(belongsToCollection: BelongsToCollection?): String {
        return gson.toJson(belongsToCollection)
    }
}