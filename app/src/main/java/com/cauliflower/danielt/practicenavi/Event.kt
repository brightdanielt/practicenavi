package com.cauliflower.danielt.practicenavi

import androidx.lifecycle.Observer

class Event<T : Any> constructor(private val content: T) {
    private var hasBeenHandled = false

    fun getContentIfNotHandled(): T? {
        if (!hasBeenHandled) {
            hasBeenHandled = true
            return content
        }
        return null
    }
}

class EventObserver<T : Any>(val handle: (T) -> Unit) : Observer<Event<T>> {
    override fun onChanged(t: Event<T>?) {
        t?.getContentIfNotHandled()?.let {
            handle(it)
        }
    }
}