package com.cauliflower.danielt.practicenavi.viewmodel

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.cauliflower.danielt.practicenavi.Event
import com.cauliflower.danielt.practicenavi.MovieRepository
import com.cauliflower.danielt.practicenavi.Result
import com.cauliflower.danielt.practicenavi.network.MovieBoundaryCallback
import com.cauliflower.danielt.practicenavi.obj.MovieItem
import kotlinx.coroutines.launch
import javax.inject.Inject

class LobbyViewModel @Inject constructor(private val movieRepository: MovieRepository) : ViewModel() {

    companion object {
        private const val DATABASE_PAGE_SIZE = 1
    }

    private var lastQueryPage: Int = 1

    private val _networkError = MutableLiveData<Result.Error>()
    val networkError: LiveData<Result.Error?> = _networkError

    val cachedMovies: LiveData<PagedList<MovieItem>?> = getPagingLiveData()

    private var _areMoviesLoading = MutableLiveData<Boolean>()
    val areMoviesLoading: LiveData<Boolean> = _areMoviesLoading

    var isEmptyMovieList: LiveData<Boolean> = Transformations.map(cachedMovies) { it?.isEmpty() }

    private val _navTask = MutableLiveData<Event<String>>()
    val navTask: LiveData<Event<String>> = _navTask

    /**
     * Get LiveData<PagedList<MovieItem>> from paging to handle network request and local query.
     * */
    private fun getPagingLiveData(): LiveData<PagedList<MovieItem>?> {
        val cachedMoviesDataSource = movieRepository.getCachedMovies()
        return LivePagedListBuilder<Int, MovieItem>(cachedMoviesDataSource, DATABASE_PAGE_SIZE)
            .setBoundaryCallback(MovieBoundaryCallback { requestAndSaveMovies() })
            .build()
    }

    private fun requestAndSaveMovies() {
        if (_areMoviesLoading.value == true) return
        _areMoviesLoading.value = true

        viewModelScope.launch {
            movieRepository.searchPopMovies(lastQueryPage).let {
                when (it) {
                    is Result.Success -> movieRepository.cacheMovies(it.data.getMovieList()).also {
                        lastQueryPage++
                        _networkError.postValue(null)
                    }
                    is Result.Error -> {
                        _networkError.postValue(it)
                        Log.i(LobbyViewModel::class.java.simpleName, "Network error:[$it]")
                    }
                }

            }
        }
        _areMoviesLoading.value = false
    }

    //Not used now.
    fun refresh() {
        _areMoviesLoading.value = false
    }

    fun navMovieDetail(movieId: String) = viewModelScope.launch {
        _navTask.value = Event(movieId)
    }
}