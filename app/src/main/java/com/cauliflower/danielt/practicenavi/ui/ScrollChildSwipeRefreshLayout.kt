package com.cauliflower.danielt.practicenavi.ui

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

class ScrollChildSwipeRefreshLayout @JvmOverloads constructor(
    context: Context,
    attributes: AttributeSet? = null
) : SwipeRefreshLayout(context, attributes) {

    var scrollUpChildView: View? = null
    override fun canScrollVertically(direction: Int): Boolean {
        return scrollUpChildView?.canScrollVertically(-1) ?: super.canScrollVertically(direction)
    }
}