package com.cauliflower.danielt.practicenavi

import java.lang.Exception

/**
 * Result of db query or network request.
 * */
sealed class Result<out R> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success -> "Success, data:[$data]"
            is Error -> "Error, exception message:[${exception.message}]"
        }
    }
}