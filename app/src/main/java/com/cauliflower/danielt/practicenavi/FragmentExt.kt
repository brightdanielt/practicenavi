package com.cauliflower.danielt.practicenavi

import android.view.View
import androidx.fragment.app.Fragment
import com.cauliflower.danielt.practicenavi.ui.ScrollChildSwipeRefreshLayout

fun Fragment.setupRefreshLayout(
    refreshLayout: ScrollChildSwipeRefreshLayout,
    scrollUpChildView: View
) {
    refreshLayout.scrollUpChildView = scrollUpChildView
}