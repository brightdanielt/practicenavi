package com.cauliflower.danielt.practicenavi.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cauliflower.danielt.practicenavi.viewmodel.LobbyViewModel
import com.cauliflower.danielt.practicenavi.databinding.MovieItemBinding
import com.cauliflower.danielt.practicenavi.obj.MovieItem

class MovieListAdapter(private val lobbyViewModel: LobbyViewModel) :
    PagedListAdapter<MovieItem, MovieListAdapter.ViewHolder>(DiffMovieList()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        item?.let {
            holder.bind(lobbyViewModel, it)
        }
    }

    class ViewHolder(private val binding: MovieItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: LobbyViewModel, movieItem: MovieItem) {
            binding.movieItem = movieItem
            binding.viewModel = viewModel
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = MovieItemBinding.inflate(inflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    class DiffMovieList : DiffUtil.ItemCallback<MovieItem>() {
        override fun areItemsTheSame(oldItem: MovieItem, newItem: MovieItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: MovieItem, newItem: MovieItem): Boolean {
            return oldItem == newItem
        }
    }
}