package com.cauliflower.danielt.practicenavi.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.cauliflower.danielt.practicenavi.MyApp
import com.cauliflower.danielt.practicenavi.R
import com.cauliflower.danielt.practicenavi.di.AppComponent
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var appComponent: AppComponent

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent = (application as MyApp).appComponent
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        val navController = findNavController(R.id.nav_host_frag)
        appBarConfiguration =
            AppBarConfiguration.Builder(
                R.id.lobby_frag_dest,
                R.id.corridor_one_frag_dest
            )
                .setDrawerLayout(drawer_layout)
                .build()
        setupActionBarWithNavController(navController, appBarConfiguration)
        nav_view.setupWithNavController(navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_frag).navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

}
