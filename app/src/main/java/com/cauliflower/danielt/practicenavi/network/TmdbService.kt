package com.cauliflower.danielt.practicenavi.network

import com.cauliflower.danielt.practicenavi.obj.MovieItem
import com.cauliflower.danielt.practicenavi.obj.MoviesWrapper
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * All queries to Tmdb api
 * */

interface TmdbService {

    @GET("/3/movie/{movieId}")
    suspend fun searchMovieById(@Path("movieId") movieId: String): Response<MovieItem>


    @GET("/3/movie/popular")
    suspend fun searchPopMovies(@Query("page") page: Int): Response<MoviesWrapper>
}