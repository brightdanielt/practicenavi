package com.cauliflower.danielt.practicenavi

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.paging.PagedList
import androidx.recyclerview.widget.RecyclerView
import com.cauliflower.danielt.practicenavi.obj.MovieItem
import com.cauliflower.danielt.practicenavi.ui.MovieListAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.squareup.picasso.Picasso

@BindingAdapter("app:items")
fun setItems(recyclerView: RecyclerView, items: PagedList<MovieItem>?) {
    items?.let{
        (recyclerView.adapter as MovieListAdapter).submitList(it)
    }
}

/**
 * @param url The full image path
 * */
@BindingAdapter("app:imgUrl")
fun setImgUrl(imageView: ImageView, url: String?) {
    url?.let { Picasso.get().load(it).into(imageView) }
}

@BindingAdapter("app:imgSrc")
fun setFabSrc(floatingActionButton: FloatingActionButton, resId: Int?) {
    resId?.let { floatingActionButton.setImageResource(it) }
}