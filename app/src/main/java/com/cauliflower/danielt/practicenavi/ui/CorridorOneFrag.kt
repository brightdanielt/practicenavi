package com.cauliflower.danielt.practicenavi.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.cauliflower.danielt.practicenavi.databinding.CorridorOneFragBinding

class CorridorOneFrag : Fragment() {

    private lateinit var binding: CorridorOneFragBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = CorridorOneFragBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }
}