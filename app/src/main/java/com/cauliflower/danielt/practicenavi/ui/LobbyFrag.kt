package com.cauliflower.danielt.practicenavi.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.cauliflower.danielt.practicenavi.EventObserver
import com.cauliflower.danielt.practicenavi.R
import com.cauliflower.danielt.practicenavi.databinding.LobbyFragBinding
import com.cauliflower.danielt.practicenavi.setupRefreshLayout
import com.cauliflower.danielt.practicenavi.viewmodel.LobbyViewModel
import com.cauliflower.danielt.practicenavi.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.lobby_frag.*
import javax.inject.Inject

class LobbyFrag : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var binding: LobbyFragBinding

    private val lobbyViewModel by viewModels<LobbyViewModel> { viewModelFactory }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as MainActivity).appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = LobbyFragBinding.inflate(inflater, container, false).apply {
            viewModel = lobbyViewModel
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.lifecycleOwner = this.viewLifecycleOwner
        setupRefreshLayout(scrollChildSwipeRefreshLayout, binding.movieList)
        setupAdapter()
        setupNavigation()
    }

    private fun setupAdapter() {
        binding.movieList.adapter = MovieListAdapter(lobbyViewModel)
    }

    private fun setupNavigation() {
        lobbyViewModel.navTask.observe(viewLifecycleOwner, EventObserver { navToMovieDetail(it) })
    }

    private fun navToMovieDetail(movieId: String) {
        //Add if to prevent user go to two destination at once, or IllegalArgument thrown.
        if (findNavController().currentDestination?.id == R.id.lobby_frag_dest) {
            LobbyFragDirections.actionLobbyToMovieDetail(movieId).also {
                findNavController().navigate(it)
            }
        }
    }
}