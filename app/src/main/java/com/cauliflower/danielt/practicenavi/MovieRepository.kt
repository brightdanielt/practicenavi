package com.cauliflower.danielt.practicenavi


import androidx.paging.DataSource
import com.cauliflower.danielt.practicenavi.network.TmdbService
import com.cauliflower.danielt.practicenavi.obj.FavoriteMovie
import com.cauliflower.danielt.practicenavi.obj.MovieItem
import com.cauliflower.danielt.practicenavi.obj.MoviesWrapper
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val movieDao: MovieDao,
    private val tmdbService: TmdbService,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : BaseRepository() {

    suspend fun searchMovieById(id: String): Result<MovieItem> = withContext(ioDispatcher) {
        return@withContext safeApiResult { tmdbService.searchMovieById(id) }
    }

    suspend fun searchPopMovies(page: Int): Result<MoviesWrapper> = withContext(ioDispatcher) {
        return@withContext safeApiResult { tmdbService.searchPopMovies(page) }
    }

    suspend fun getFavoriteMovies(): List<FavoriteMovie> = withContext(ioDispatcher) {
        movieDao.getFavoriteMovies()
    }

    suspend fun isFavoriteMovie(movieId: String): Int = withContext(ioDispatcher) {
        movieDao.isFavoriteMovie(movieId)
    }

    suspend fun addFavoriteMovie(favoriteMovie: FavoriteMovie) = withContext(ioDispatcher) {
        movieDao.addFavoriteMovie(favoriteMovie)
    }

    suspend fun removeFavoriteMovie(movieId: String) = withContext(ioDispatcher) {
        movieDao.removeFavoriteMovie(movieId)
    }

    suspend fun cacheMovies(movies: List<MovieItem>) = withContext(ioDispatcher) {
        movieDao.cacheMovies(movies)
    }

    fun getCachedMovies(): DataSource.Factory<Int, MovieItem> = movieDao.getCachedMovies()

}

open class BaseRepository {
    /**
     * Handle every exception into @link[Result.Error]
     * Not bad : )
     * */
    protected suspend fun <T> safeApiResult(call: suspend () -> Response<T>): Result<T> {
        val response: Response<T>
        try {
            response = call.invoke()
        } catch (exception: Exception) {
            //This is an unexpected exception for a developer 😟
            return Result.Error(Exception("Exception occurred while invoking the call, message:${exception.message}."))
        }
        response.run {
            //T is non-bull so we add !!
            return if (isSuccessful) Result.Success(body()!!)
            else {
                //todo inappropriate blocking method call
                val errorBody: String? = errorBody()?.string()
                Result.Error(IOException("Response is not successful. ErrorBody:${errorBody ?: "null"}"))
            }
        }
    }
}