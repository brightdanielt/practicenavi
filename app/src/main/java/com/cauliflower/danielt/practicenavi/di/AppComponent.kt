package com.cauliflower.danielt.practicenavi.di

import android.content.Context
import com.cauliflower.danielt.practicenavi.ui.LobbyFrag
import com.cauliflower.danielt.practicenavi.ui.MovieDetailsFrag
import com.cauliflower.danielt.practicenavi.viewmodel.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/**
 * To keep this instance alive with application
 * */
@Singleton
@Component(modules = [AppModule::class,ViewModelModule::class])
interface AppComponent {

    //With @BindsInstance, the context passed in will be available in graph.
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): AppComponent
    }

    fun inject(activity: LobbyFrag)
    fun inject(activity: MovieDetailsFrag)
}