package com.cauliflower.danielt.practicenavi.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.cauliflower.danielt.practicenavi.databinding.MovieDetailFragBinding
import com.cauliflower.danielt.practicenavi.setupRefreshLayout
import com.cauliflower.danielt.practicenavi.viewmodel.MovieDetailsViewModel
import com.cauliflower.danielt.practicenavi.viewmodel.ViewModelFactory
import javax.inject.Inject

class MovieDetailsFrag : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var binding: MovieDetailFragBinding
    private val args by navArgs<MovieDetailsFragArgs>()
    private val viewModel by viewModels<MovieDetailsViewModel> { viewModelFactory }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as MainActivity).appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = MovieDetailFragBinding.inflate(inflater, container, false).also {
            it.viewModel = this.viewModel
            it.lifecycleOwner = this.viewLifecycleOwner
        }
        viewModel.loadMovie(args.movieId)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupRefreshLayout(binding.refreshLayout, binding.movieDetailChildView)
    }
}