package com.cauliflower.danielt.practicenavi

import android.app.Application
import com.cauliflower.danielt.practicenavi.di.AppComponent
import com.cauliflower.danielt.practicenavi.di.DaggerAppComponent


class MyApp : Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.factory().create(applicationContext)
    }
}